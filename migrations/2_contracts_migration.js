const erc20Token = artifacts.require("./ERC20Token.sol");

module.exports = function(deployer) {
    deployer.deploy(erc20Token, 1000000000, "SHVTest Token", 8, "SHVTest");
};